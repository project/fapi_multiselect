<?php

/**
 * Trying callback function.
 */
function fapi_multiselect_try() {
  // Need devel_create_greeking() function to generate dummu content
  module_load_include('inc', 'devel', 'devel_generate');

  if (isset($_GET['op']) && $_GET['op'] === 'sourcecode') {
    drupal_set_message(l('View form', $_GET['q']));
    return highlight_string(file_get_contents(__FILE__));
  }

  drupal_set_message(l('View source code', $_GET['q'], array('query' => 'op=sourcecode')));

  return drupal_get_form('fapi_multiselect_test_form');
}

function fapi_multiselect_test_form() {
  $form['radom_text'] = array(
    '#type' => 'markup',
    '#value' => '<div>' .devel_create_greeking(50) . '</div><br />'
  );

  $form['select1'] = array(
    '#type' => 'fapi_multiselect',
    '#title' => 'Widget #1',
    '#required' => TRUE,
    '#options' => array(),
    '#default_value' => array(5, 7, 9),
  );

  for ($i = 0; $i <= 50; ++$i) {
    $form['select1']['#options'][$i] = '(val: '. $i .') ' .devel_create_greeking(rand(1, 2));
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );

  $form['submit_error'] = array(
    '#type' => 'submit',
    '#value' => 'Submit with error',
  );

  return $form;
}

/**
 * Validate form's submitted data.
 */
function fapi_multiselect_test_form_validate(&$form, &$form_state) {
  if ($form_state['clicked_button']['#value'] === 'Submit with error') {
    form_set_error('select1', '<strong>Error:</strong> ' . devel_create_greeking(rand(1, 5)));
  }
}

function fapi_multiselect_test_form_submit(&$form, &$form_state) {
  drupal_set_message('Submitted values: ' . implode(', ', $form_state['values']['select1']));
}
