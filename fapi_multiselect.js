/**
 * The JavaScript behavior that goes with the Multiselect form element.
 */

Drupal.behaviors.fapi_multiselect = function(context) {
  var query = '.form-fapi-multiselect:not(.fapi_multiselect-processed)';
  $(query, context).addClass('fapi_multiselect-processed').each(function(){
    var idWrapper = $(this).attr('id');
    var idLeft    = idWrapper + '-left';
    var idRight   = idWrapper + '-right';
    var $wrapper  = $(this);
    var $select   = $wrapper.find('select:eq(0)').hide();
    var $options  = $select.find('option');

    var pane = '<div class="fapi-multiselect-left" id=":id" style="float: left; width: 40%;">'
        + '<div><label>:label:</label></div>'
        + '<select multiple="multiple" size="10" style="min-width: 120px;" class="form-select"></select>'
      + '</div>';

    $wrapper.prepend(
      '<div class="fapi-multiselect-wrapper">'
        + pane.replace(':id', idLeft).replace(':label', 'Available Options')
        + '<div class="fapi-multiselect-controlers" style="float: left;">'
          + '<ul>'
            + '<li><a href="#" class="fapi-multiselect-add">Add</a></li>'
            + '<li><a href="#" class="fapi-multiselect-remove">Remove</a></li>'
          + '</ul>'
        + '</div>'
        + pane.replace(':id', idRight).replace(':label', 'Selected Options')
        + '<div style="clear: both;">&nbsp;</div>'
      + '</div>'
    );

    var $leftSelect  = $('#' + idLeft  + ' select');
    var $rightSelect = $('#' + idRight + ' select');

    function updatePanes() {
      $leftSelect.find('option').remove();
      $rightSelect.find('option').remove();

      $options.each(function(){
        var $element = $(this).clone().attr('selected', '');

        if ($(this).is(':selected')) {
          $element.appendTo('#' + idWrapper + '-right > select');
        }
        else {
          $element.appendTo('#' + idWrapper + '-left > select');
        }
      });
    }

    function updateMainSelect() {
      // Get selected values
      var values = [];

      $rightSelect.find('option').each(function(){
        values.push($(this).val());
      });

      $select.val(values);
    }

    $select.change(function(){
      updatePanes();
    });

    updatePanes();

    // Moves selection if it's double clicked to selected box
    $rightSelect.dblclick(function() {
      $rightSelect.moveSelectionTo($leftSelect);
      updateMainSelect();
    });

    // Moves selection if it's double clicked to unselected box
    $leftSelect.dblclick(function() {
      $leftSelect.moveSelectionTo($rightSelect);
      updateMainSelect();
    });

    // Moves selection if add is clicked to selected box
    $wrapper.find('ul li a:eq(0)').click(function() {
      event.preventDefault();
      $leftSelect.moveSelectionTo($rightSelect);
      updateMainSelect();
    });

    // Moves selection if remove is clicked to selected box
    $wrapper.find('ul li a:eq(1)').click(function() {
      event.preventDefault();
      $rightSelect.moveSelectionTo($leftSelect);
      updateMainSelect();
    });
  });
};

// Selects all the items in the select box it is called from.
// usage $('nameofselectbox').selectAll();
jQuery.fn.selectAll = function() {
  this.each(function() {
    for (var x=0;x<this.options.length;x++) {
      option = this.options[x];
      option.selected = true;
    }
  });
}

// Removes the content of this select box from the target
// usage $('nameofselectbox').removeContentsFrom(target_selectbox)
jQuery.fn.removeContentsFrom = function() {
  dest = arguments[0];
  this.each(function() {
    for (var x=this.options.length-1;x>=0;x--) {
      dest.removeOption(this.options[x].value);
    }
  });
}

// Moves the selection to the select box specified
// usage $('nameofselectbox').moveSelectionTo(destination_selectbox)
jQuery.fn.moveSelectionTo = function() {
  dest = arguments[0];
  this.each(function() {
    for (var x=0; x < this.options.length; x++) {
      option = this.options[x];
      if (option.selected) {
        dest.addOption(option);
        this.remove(x);
        $(this).triggerHandler('option-removed', option);
        x--; // Move x back one so that we'll successfully check again to see if it's selected.
      }
    }
  });
}

// Adds an option to a select box
// usage $('nameofselectbox').addOption(optiontoadd);
jQuery.fn.addOption = function() {
  option = arguments[0];
  this.each(function() {
    //had to alter code to this to make it work in IE
    anOption = document.createElement('option');
    anOption.text = option.text;
    anOption.value = option.value;
    this.options[this.options.length] = anOption;
    $(this).triggerHandler('option-added', anOption);
    return false;
  });
}

// Removes an option from a select box
// usage $('nameofselectbox').removeOption(valueOfOptionToRemove);
jQuery.fn.removeOption = function() {
  targOption = arguments[0];
  this.each(function() {
    for (var x=this.options.length-1;x>=0;x--) {
      option = this.options[x];
      if (option.value==targOption) {
        this.remove(x);
        $(this).triggerHandler('option-removed', option);
      }
    }
  });
}
